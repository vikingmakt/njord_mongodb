from njord_mongodb.codec import dictfy
from rask.base import Base
from rask.options import options
from rask.rmq import ack

__all__ = ['Insert']

class Insert(Base):
    def __init__(self,channel,db):
        self.db = db
        channel.result().channel.basic_consume(
            consumer_callback=self.on_msg,
            queue=options.mongodb['services']['insert']['queue']
        )
        self.log.info('started')

    def fetch(self,result,headers,ack):
        def on_fetch(_):
            try:
                body = {
                    '_id':_.result(),
                    'ok':True
                }
            except Exception as ex:
                body = {
                    '_id':None,
                    'ok':False,
                    'err':unicode(ex)
                }
            except:
                raise

            options.mongodb['push'](
                body=body,
                headers=headers,
                ack=ack
            )
            return True

        self.ioengine.loop.add_future(result,on_fetch)
        return True

    def insert(self,msg,headers,ack):
        try:
            self.ioengine.loop.add_callback(
                self.fetch,
                result=self.db[headers['db']][headers['collection']].insert(**msg),
                headers=headers,
                ack=ack
            )
        except Exception as ex:
            options.mongodb['push'](
                body={
                    '_id':None,
                    'ok':False,
                    'err':unicode(ex)
                },
                headers=headers,
                ack=ack
            )
        except:
            raise
        return True

    def on_msg(self,channel,method,properties,body):
        self.ioengine.loop.add_callback(
            self.validate,
            msg=dictfy(body),
            headers=properties.headers,
            ack=self.ioengine.future(ack(channel,method))
        )
        return True

    def validate(self,msg,headers,ack):
        try:
            assert headers['db'] and isinstance(headers['db'],(str,unicode))
            assert headers['collection'] and isinstance(headers['collection'],(str,unicode))
        except (AssertionError,KeyError,TypeError):
            ack.set_result(True)
        except:
            raise
        else:
            self.ioengine.loop.add_callback(
                self.insert,
                msg=msg,
                headers=headers,
                ack=ack
            )
        return True
