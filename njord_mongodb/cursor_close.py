from rask.base import Base
from rask.options import options
from rask.rmq import ack

__all__ = ['CursorClose']

class CursorClose(Base):
    def __init__(self,channel):
        channel.result().channel.basic_consume(
            consumer_callback=self.on_msg,
            queue=options.mongodb['services']['cursor_close']['queue']
        )
        self.log.info('started')

    def cursor(self,cursor_id,ack=None):
        try:
            assert options.mongodb['cursors'].check(cursor_id)
        except AssertionError:
            self.finish(ack)
        except:
            raise
        else:
            self.ioengine.loop.add_callback(
                self.close,
                cursor=options.mongodb['cursors'].get(cursor_id),
                cursor_id=cursor_id,
                ack=ack
            )
        return True

    def close(self,cursor,cursor_id,ack):
        def on_close(result):
            options.mongodb['cursors'].remove(cursor_id)
            self.finish(ack)
            return True

        self.ioengine.loop.add_future(cursor.close(),on_close)
        return True

    def finish(self,ack):
        try:
            ack.set_result(True)
        except AttributeError:
            pass
        except:
            raise
        return True

    def on_msg(self,channel,method,properties,body):
        self.ioengine.loop.add_callback(
            self.cursor,
            cursor_id=body,
            ack=self.ioengine.future(ack(channel,method))
        )
        return True
