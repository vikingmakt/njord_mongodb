from njord_mongodb.codec import jsonify
from rask.base import Base
from rask.options import options
from rask.rmq import BasicProperties

__all__ = ['Push']

class Push(Base):
    def __init__(self,channel):
        self.channel = channel.result().channel
        self.log.info('started')

    def publish(self,body,headers,ack):
        headers['mongodb-response'] = True
        self.channel.basic_publish(
            body=jsonify(body),
            exchange=options.mongodb['exchange']['topic'],
            routing_key='',
            properties=BasicProperties(headers=headers)
        )
        ack.set_result(True)
        return True
