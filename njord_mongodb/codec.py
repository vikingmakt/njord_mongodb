# -*- coding: utf-8 -*-
from bson import json_util
from simplejson import dumps,loads
from rask.options import options

__all__ = [
    'cursor_ns',
    'dictfy',
    'jsonify',
    'service_ns'
]

def cursor_ns(_):
    return _ % (options.mongodb['name'],options.mongodb['uid'])

def dictfy(_):
    try:
        return loads(_,object_hook=json_util.object_hook)
    except:
        return False

def jsonify(_):
    try:
        return dumps(_,default=json_util.default,separators=(',',':'))
    except:
        return False

def service_ns(_):
    return _ % options.mongodb['name']
