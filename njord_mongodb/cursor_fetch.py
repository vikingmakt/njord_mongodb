from rask.base import Base
from rask.options import options
from rask.rmq import ack

__all__ = ['CursorFetch']

class CursorFetch(Base):
    def __init__(self,channel,services):
        self.services = services
        channel.result().channel.basic_consume(
            consumer_callback=self.on_msg,
            queue=options.mongodb['services']['cursor_fetch']['queue']
        )
        self.log.info('started')

    def cursor(self,cursor_id,headers,ack):
        try:
            assert options.mongodb['cursors'].check(cursor_id)
        except AssertionError:
            options.mongodb['push'](
                body={
                    'doc':None,
                    'ok':False,
                    'err':'Cursor not found'
                },
                headers=headers,
                ack=ack
            )
        except:
            raise
        else:
            self.ioengine.loop.add_callback(
                self.fetch,
                cursor_id=cursor_id,
                headers=headers,
                ack=ack,
                cursor=options.mongodb['cursors'].get(cursor_id)
            )
        return True

    def fetch(self,cursor_id,headers,ack,cursor):
        def on_fetch(_):
            try:
                assert _.result()
            except AssertionError:
                body = {
                    'doc':None,
                    'ok':True
                }
                self.ioengine.loop.add_callback(self.services['cursor_close'].cursor,cursor_id)
            except Exception as ex:
                body = {
                    'doc':None,
                    'ok':False,
                    'err':unicode(ex)
                }
            except:
                raise
            else:
                body = {
                    'doc':cursor.next_object(),
                    'ok':True
                }

            options.mongodb['push'](
                body=body,
                headers=headers,
                ack=ack
            )
            return True

        self.ioengine.loop.add_future(cursor.fetch_next,on_fetch)
        return True

    def on_msg(self,channel,method,properties,body):
        self.ioengine.loop.add_callback(
            self.cursor,
            cursor_id=body,
            headers=properties.headers,
            ack=self.ioengine.future(ack(channel,method))
        )
        return True
