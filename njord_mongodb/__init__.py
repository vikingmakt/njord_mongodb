from aggregate import Aggregate
from cursor_close import CursorClose
from cursor_fetch import CursorFetch
from cursors import Cursors
from find import Find
from find_and_modify import FindAndModify
from find_one import FindOne
from insert import Insert
import motor
from motor import MotorClient
from njord_mongodb.codec import cursor_ns,service_ns
from push import Push
from rask.main import Main
from rask.options import define,options
from rask.parser.json import dictfy
from rask.rmq import Announce,RMQ
from remove import Remove
from save import Save
from update import Update
from tornado import autoreload

__all__ = ['Run']

class Run(Main):
    @property
    def db(self):
        try:
            assert self.__db
        except (AssertionError,AttributeError):
            self.__db = MotorClient(
                **options.mongodb['client']
            )
            self.log.info('Connected to %s' % options.mongodb['client']['host'])
        except:
            raise
        return self.__db

    @property
    def rmq(self):
        try:
            assert self.__rmq
        except (AssertionError,AttributeError):
            def push(_):
                options.mongodb['push'] = Push(_).publish
                return True

            self.__rmq = RMQ(url=options.mongodb['rmq'])
            self.__rmq.channel('push',self.ioengine.future(push))
        except:
            raise
        return self.__rmq

    @property
    def settings(self):
        try:
            assert self.__settings
        except (AssertionError,AttributeError):
            self.__settings = {
                'cursors':Cursors(),
                'exchange':{
                    'topic':'njord_mongodb',
                    'headers':'njord_mongodb_headers'
                },
                'services':{
                    'aggregate':{
                        'queue':service_ns('njord_mongodb_aggregate_%s'),
                        'rk':service_ns('njord.mongodb.aggregate.%s')
                    },
                    'cursor_close':{
                        'durable':False,
                        'exclusive':True,
                        'queue':cursor_ns('njord_mongodb_cursor_close_%s_%s'),
                        'rk':cursor_ns('njord.mongodb.cursor.close.%s.%s')
                    },
                    'cursor_fetch':{
                        'durable':False,
                        'exclusive':True,
                        'queue':cursor_ns('njord_mongodb_cursor_fetch_%s_%s'),
                        'rk':cursor_ns('njord.mongodb.cursor.fetch.%s.%s')
                    },
                    'find':{
                        'queue':service_ns('njord_mongodb_find_%s'),
                        'rk':service_ns('njord.mongodb.find.%s')
                    },
                    'find_and_modify':{
                        'queue':service_ns('njord_mongodb_find_and_modify_%s'),
                        'rk':service_ns('njord.mongodb.find_and_modify.%s')
                    },
                    'find_one':{
                        'queue':service_ns('njord_mongodb_find_one_%s'),
                        'rk':service_ns('njord.mongodb.find_one.%s')
                    },
                    'insert':{
                        'queue':service_ns('njord_mongodb_insert_%s'),
                        'rk':service_ns('njord.mongodb.insert.%s')
                    },
                    'remove':{
                        'queue':service_ns('njord_mongodb_remove_%s'),
                        'rk':service_ns('njord.mongodb.remove.%s')
                    },
                    'save':{
                        'queue':service_ns('njord_mongodb_save_%s'),
                        'rk':service_ns('njord.mongodb.save.%s')
                    },
                    'update':{
                        'queue':service_ns('njord_mongodb_update_%s'),
                        'rk':service_ns('njord.mongodb.update.%s')
                    }
                }
            }
        except:
            raise
        return self.__settings

    def after(self):
        self.log.info('Njord Mongodb')
        try:
            assert options.autoreload
        except AssertionError:
            pass
        except:
            raise
        else:
            autoreload.watch('settings.json')
            autoreload.start()
            self.log.info('autoreload:start')
        return True

    def before(self):
        self.ioengine.loop.add_callback(self.settings_load)
        return True

    def services_init(self,_):
        def aggregate(_):
            self.services['aggregate'] = Aggregate(
                channel=_,
                db=self.db
            )
            return True

        def cursor_close(_):
            self.services['cursor_close'] = CursorClose(
                channel=_
            )
            return True

        def cursor_fetch(_):
            self.services['cursor_fetch'] = CursorFetch(
                channel=_,
                services=self.services
            )
            return True

        def find(_):
            self.services['find'] = Find(
                channel=_,
                db=self.db
            )
            return True

        def find_and_modify(_):
            self.services['find_and_modify'] = FindAndModify(
                channel=_,
                db=self.db
            )
            return True

        def find_one(_):
            self.services['find_one'] = FindOne(
                channel=_,
                db=self.db
            )
            return True

        def insert(_):
            self.services['insert'] = Insert(
                channel=_,
                db=self.db
            )
            return True

        def remove(_):
            self.services['remove'] = Remove(
                channel=_,
                db=self.db
            )
            return True

        def save(_):
            self.services['save'] = Save(
                channel=_,
                db=self.db
            )
            return True

        def update(_):
            self.services['update'] = Update(
                channel=_,
                db=self.db
            )
            return True

        self.rmq.channel('aggregate',future=self.ioengine.future(aggregate))
        self.rmq.channel('cursor_close',future=self.ioengine.future(cursor_close),prefetch=1)
        self.rmq.channel('cursor_fetch',future=self.ioengine.future(cursor_fetch),prefetch=1)
        self.rmq.channel('find',future=self.ioengine.future(find))
        self.rmq.channel('find_and_modify',future=self.ioengine.future(find_and_modify))
        self.rmq.channel('find_one',future=self.ioengine.future(find_one))
        self.rmq.channel('insert',future=self.ioengine.future(insert))
        self.rmq.channel('remove',future=self.ioengine.future(remove))
        self.rmq.channel('save',future=self.ioengine.future(save))
        self.rmq.channel('update',future=self.ioengine.future(update))
        return True

    def settings_check(self):
        try:
            assert options.mongodb['name']
            assert options.mongodb['client']
            assert options.mongodb['client']['host']
            assert options.mongodb['rmq']
        except (AssertionError,KeyError,TypeError):
            self.log.error('Missing setting')
            self.ioengine.stop()
        except:
            raise
        else:
            def announce(_):
                Announce(
                    channel=_,
                    settings=options.mongodb,
                    future=self.ioengine.future(self.services_init)
                )
                return True

            self.rmq.channel('announce',future=self.ioengine.future(announce))
        return True

    def settings_load(self):
        try:
            with open('settings.json') as rfile:
                rconf = dictfy(rfile.read())
            assert rconf
        except AssertionError:
            self.log.error('Invalid settings file')
            self.ioengine.stop()
        except IOError:
            self.log.error('Missing settings file')
            self.ioengine.stop()
        except:
            raise
        else:
            rconf['uid'] = self.uuid
            define('mongodb',default=rconf)
            options.mongodb.update(self.settings)
            self.ioengine.loop.add_callback(self.settings_check)
        return True
