from njord_mongodb.codec import dictfy
from rask.base import Base
from rask.options import options
from rask.rmq import ack

__all__ = ['Aggregate']

class Aggregate(Base):
    def __init__(self,channel,db):
        self.db = db
        channel.result().channel.basic_consume(
            consumer_callback=self.on_msg,
            queue=options.mongodb['services']['aggregate']['queue']
        )
        self.log.info('started')

    def aggregate(self,msg,headers,ack):
        try:
            self.ioengine.loop.add_callback(
                self.fetch_one,
                cursor=self.db[headers['db']][headers['collection']].aggregate(cursor={},**msg),
                headers=headers,
                ack=ack
            )
        except Exception as ex:
            options.mongodb['push'](
                body={
                    'ok':False,
                    'err':unicode(ex),
                    'cursor':None
                },
                headers=headers,
                ack=ack
            )
        except:
            raise
        return True

    def fetch_one(self,cursor,headers,ack):
        def on_fetch(_):
            try:
                assert _.result()
            except AssertionError:
                body = {
                    'doc':None,
                    'ok':True,
                    'cursor':None
                }
            except Exception as ex:
                body = {
                    'ok':False,
                    'err':unicode(ex),
                    'cursor':None
                }
            except:
                raise
            else:
                body = {
                    'doc':cursor.next_object(),
                    'ok':True,
                    'cursor':{
                        'id':options.mongodb['cursors'].add(cursor),
                        'uid':options.mongodb['uid']
                    }
                }

            options.mongodb['push'](
                body=body,
                headers=headers,
                ack=ack
            )
            return True

        self.ioengine.loop.add_future(cursor.fetch_next,on_fetch)
        return True

    def on_msg(self,channel,method,properties,body):
        self.ioengine.loop.add_callback(
            self.validate,
            msg=dictfy(body),
            headers=properties.headers,
            ack=self.ioengine.future(ack(channel,method))
        )
        return True

    def validate(self,msg,headers,ack):
        try:
            assert headers['db'] and isinstance(headers['db'],(str,unicode))
            assert headers['collection'] and isinstance(headers['collection'],(str,unicode))
        except (AssertionError,KeyError,TypeError):
            ack.set_result(True)
        except:
            raise
        else:
            self.ioengine.loop.add_callback(
                self.aggregate,
                msg=msg,
                headers=headers,
                ack=ack
            )
        return True
