from hashlib import sha1
from rask.base import Base

__all__ = ['Cursors']

class Cursors(Base):
    def __init__(self):
        self.objs = {}

    def __uid__(self):
        return sha1('<cursor:%s>' % self.ioengine.uuid4).hexdigest()

    def add(self,cursor):
        uid = self.__uid__()
        self.objs[uid] = cursor
        self.log.debug('Cursor %s created' % uid)
        return uid

    def check(self,uid):
        return uid in self.objs

    def get(self,uid):
        try:
            assert uid in self.objs
        except AssertionError:
            return None
        except:
            raise
        return self.objs[uid]

    def remove(self,uid):
        try:
            del self.objs[uid]
        except KeyError:
            pass
        except:
            raise
        else:
            self.log.debug('Cursor %s cleared' % uid)
        return True
