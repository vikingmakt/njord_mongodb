# Njord Mongodb

+ [Aggregate](#aggregate)
+ [Find](#find)
+ [Find And Modify](#find-and-modify)
+ [Find One](#find-one)
+ [Insert](#insert)
+ [Remove](#remove)
+ [Save](#save)
+ [Update](#update)
+ Cursor
    + [Cursor Close](#cursor-close)
    + [Cursor Fetch](#cursor-fetch)

---

## Aggregate

### Ask

+ exchange: **njord_mongodb**
+ rk: **njord.mongodb.aggregate.{cluster name}**
+ headers:
    + db: **{db name}**
    + collection: **{collection name}**
+ body:
    + [params](http://motor.readthedocs.io/en/stable/api-tornado/motor_collection.html#motor.motor_tornado.MotorCollection.aggregate)

### Answer

+ exchange: **njord_mongodb_headers**
+ headers:
    + mongodb-response: **true**
+ body:

#### Ok

```json
{
    "doc": {first iteration},
    "ok": true,
    "cursor": {
        "id": {cursor id},
        "uid": {njord id}
    }
}
```

#### Empty cursor

```json
{
    "doc": null,
    "ok": true,
    "cursor": null
}
```

#### Error

```json
{
    "doc": null,
    "ok": false,
    "err": {error text},
    "cursor": null
}
```

---

## Find

### Ask

+ exchange: **njord_mongodb**
+ rk: **njord.mongodb.find.{cluster name}**
+ headers:
    + db: **{db name}**
    + collection: **{collection name}**
+ body:
    + [params](http://motor.readthedocs.io/en/stable/api-tornado/motor_collection.html#motor.motor_tornado.MotorCollection.find)

### Answer

+ exchange: **njord_mongodb_headers**
+ headers:
    + mongodb-response: **true**
+ body:

#### Ok

```json
{
    "doc": {first iteration},
    "ok": true,
    "cursor": {
        "id": {cursor id},
        "uid": {njord id}
    }
}
```

#### Empty cursor

```json
{
    "doc": null,
    "ok": true,
    "cursor": null
}
```

#### Error

```json
{
    "doc": null,
    "ok": false,
    "err": {error text},
	"cursor": null
}
```

---

## Find And Modify

### Ask

+ exchange: **njord_mongodb**
+ rk: **njord.mongodb.find_and_modify.{cluster name}**
+ headers:
    + db: **{db name}**
    + collection: **{collection name}**
+ body:
    + [params](http://motor.readthedocs.io/en/stable/api-tornado/motor_collection.html#motor.motor_tornado.MotorCollection.find_and_modify)

### Answer

+ exchange: **njord_mongodb_headers**
+ headers:
    + mongodb-response: **true**
+ body:

#### Ok

```json
{
    "doc": {query result},
    "ok": true
}
```

#### Not found

```json
{
    "doc": null,
    "ok": true
}
```

#### Error

```json
{
    "doc": null,
    "ok": false,
    "err": {error text}
}
```

---

## Find One

### Ask

+ exchange: **njord_mongodb**
+ headers:
    + db: **{db name}**
    + collection: **{collection name}**
+ body:
    + [params](http://motor.readthedocs.io/en/stable/api-tornado/motor_collection.html#motor.motor_tornado.MotorCollection.find_one)

### Answer

+ exchange: **njord_mongodb_headers**
+ headers:
    + mongodb-response: **true**
+ body:

#### Ok

```json
{
    "doc": {query result},
    "ok": true
}
```

#### Not found

```json
{
    "doc": null,
    "ok": true
}
```

#### Error

```json
{
    "doc": null,
    "ok": false,
    "err": {error text}
}
```

---

## Insert

### Ask

+ exchange: **njord_mongodb**
+ rk: **njord.mongodb.insert.{cluster name}**
+ headers:
    + db: **{db name}**
    + collection: **{collection name}**
+ body:
    + [params](http://motor.readthedocs.io/en/stable/api-tornado/motor_collection.html#motor.motor_tornado.MotorCollection.insert)

### Answer

+ exchange: **njord_mongodb_headers**
+ headers:
    + mongodb-response: **true**
+ body:

#### Ok

```json
{
    "_id": {obj id},
    "ok": true
}
```

#### Error

```json
{
    "_id": null,
    "ok": false,
    "err": {error text}
}
```

---

## Remove

### Ask

+ exchange: **njord_mongodb**
+ rk: **njord.mongodb.remove.{cluster name}**
+ headers:
    + db: **{db name}**
    + collection: **{collection name}**
+ body:
    + [params](http://motor.readthedocs.io/en/stable/api-tornado/motor_collection.html#motor.motor_tornado.MotorCollection.remove)

### Answer

+ exchange: **njord_mongodb_headers**
+ headers:
    + mongodb-response: **true**
+ body:

#### Ok

```json
{
    "result": {result info},
    "ok": true
}
```

#### Error

```json
{
    "result": null,
    "ok": false,
    "err": {error text}
}
```

---

## Save

### Ask

+ exchange: **njord_mongodb**
+ rk: **njord.mongodb.save.{cluster name}**
+ headers:
    + db: **{db name}**
    + collection: **{collection name}**
+ body:
    + [params](http://motor.readthedocs.io/en/stable/api-tornado/motor_collection.html#motor.motor_tornado.MotorCollection.save)

### Answer

+ exchange: **njord_mongodb_headers**
+ headers:
    + mongodb-response: **true**
+ body:

#### Ok

```json
{
    "_id": {obj id},
    "ok": true
}
```

#### Error

```json
{
    "_id": null,
    "ok": false,
    "err": {error text}
}
```

---

## Update

### Ask

+ exchange: **njord_mongodb**
+ rk: **njord.mongodb.update.{cluster name}**
+ headers:
    + db: **{db name}**
    + collection: **{collection name}**
+ body:
    + [params](http://motor.readthedocs.io/en/stable/api-tornado/motor_collection.html#motor.motor_tornado.MotorCollection.update)

### Answer

+ exchange: **njord_mongodb_headers**
+ headers:
    + mongodb-response: **true**
+ body:

#### Ok

```json
{
    "result": {result info},
    "ok": true
}
```

#### Error

```json
{
    "result": null,
    "ok": false,
    "err": {error text}
}
```

---

## Cursor Close

### Ask

+ exchange: **njord_mongodb**
+ rk: **njord.mongodb.cursor.close.{cluster name}.{njord uid}**
+ body:
    {cursor id}

---

## Cursor Fetch

### Ask

+ exchange: **njord_mongodb**
+ rk: **njord.mongodb.cursor.fetch.{cluster name}.{njord uid}**
+ body:
    {cursor id}

### Answer

+ exchange: **njord_mongodb_headers**
+ headers:
    + mongodb-response: **true**
+ body:

#### Ok

```json
{
    "doc": {doc},
    "ok": true
}
```

#### Empty

```json
{
    "doc": null,
    "ok": true
}
```

#### Cursor not found

```json
{
    "doc": null,
    "ok": false
}
```

#### Error

```json
{
    "doc": null,
    "ok": false,
    "err": {error text}
}
```
